package com.example.home.calc;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
public class FragmentAdvanced extends Fragment implements View.OnClickListener {
    private static TextView textview;
    View view;
    Button btn,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnplus,btn11,btnminus,btnmnoz,btndziel,btnbksp,btnC,btn18,btnsqrt,btnx2,btnxy,btn10,btnln,btnsin,btncos,btntan;
    String text="",text2="",znak="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.advanced, container, false);
        textview=((TextView) view.findViewById(R.id.textAdvanced));

        btn = (Button)view.findViewById(R.id.buttonAdvanced0);
        btn.setOnClickListener(this);
        btn1 = (Button)view.findViewById(R.id.buttonAdvanced1);
        btn1.setOnClickListener(this);
        btn2 = (Button)view.findViewById(R.id.buttonAdvanced2);
        btn2.setOnClickListener(this);
        btn3 = (Button)view.findViewById(R.id.buttonAdvanced3);
        btn3.setOnClickListener(this);
        btn4 = (Button)view.findViewById(R.id.buttonAdvanced4);
        btn4.setOnClickListener(this);
        btn5 = (Button)view.findViewById(R.id.buttonAdvanced5);
        btn5.setOnClickListener(this);
        btn6 = (Button)view.findViewById(R.id.buttonAdvanced6);
        btn6.setOnClickListener(this);
        btn7 = (Button)view.findViewById(R.id.buttonAdvanced7);
        btn7.setOnClickListener(this);
        btn8 = (Button)view.findViewById(R.id.buttonAdvanced8);
        btn8.setOnClickListener(this);
        btn9 = (Button)view.findViewById(R.id.buttonAdvanced9);
        btn9.setOnClickListener(this);
        btnplus = (Button)view.findViewById(R.id.buttonAdvancedplus);
        btnplus.setOnClickListener(this);
        btn11 = (Button)view.findViewById(R.id.buttonAdvanced11);
        btn11.setOnClickListener(this);
        btnminus = (Button)view.findViewById(R.id.buttonAdvancedminus);
        btnminus.setOnClickListener(this);
        btnmnoz = (Button)view.findViewById(R.id.buttonAdvancedmnoz);
        btnmnoz.setOnClickListener(this);
        btndziel= (Button)view.findViewById(R.id.buttonAdvanceddziel);
        btndziel.setOnClickListener(this);
        btnbksp= (Button)view.findViewById(R.id.buttonAdvancedbksp);
        btnbksp.setOnClickListener(this);
        btnC= (Button)view.findViewById(R.id.buttonAdvancedC);
        btnC.setOnClickListener(this);
        btn18= (Button)view.findViewById(R.id.buttonAdvanced18);
        btn18.setOnClickListener(this);
        btnsqrt= (Button)view.findViewById(R.id.buttonAdvancedsqrt);
        btnsqrt.setOnClickListener(this);
        btnx2= (Button)view.findViewById(R.id.buttonAdvancedx2);
        btnx2.setOnClickListener(this);
        btnxy= (Button)view.findViewById(R.id.buttonAdvancedxy);
        btnxy.setOnClickListener(this);
        btn10= (Button)view.findViewById(R.id.buttonAdvanced10);
        btn10.setOnClickListener(this);
        btnln= (Button)view.findViewById(R.id.buttonAdvancedln);
        btnln.setOnClickListener(this);
        btnsin= (Button)view.findViewById(R.id.buttonAdvancedsin);
        btnsin.setOnClickListener(this);
        btncos= (Button)view.findViewById(R.id.buttonAdvancedcos);
        btncos.setOnClickListener(this);
        btntan= (Button)view.findViewById(R.id.buttonAdvancedtan);
        btntan.setOnClickListener(this);

        return view;
    }
    private void dodaj(){
        float skladnik1,skladnik2;
        if(text=="")
            skladnik1=0;
        else
            skladnik1 = Float.parseFloat(text);
        if (text2=="")
            skladnik2=0;
        else
            skladnik2 = Float.parseFloat(text2);

        text=String.valueOf(skladnik1+skladnik2);
    }
    private void minus(){
        float odjemna ,odjemnik;
        if(text=="")
            odjemna =0;
        else
            odjemna  = Float.parseFloat(text2);
        if (text2=="")
            odjemnik=0;
        else
            odjemnik = Float.parseFloat(text);

        text=String.valueOf(odjemna-odjemnik);
    }
    private void mnoz(){
        float czynnik1 ,czynnik2;
        if(text=="")
            czynnik1 =0;
        else
            czynnik1  =Float.parseFloat(text2);
        if (text2=="")
            czynnik2=0;
        else
            czynnik2 = Float.parseFloat(text);

        text=String.valueOf(czynnik1*czynnik2);
    }
    private void dziel(){
        float dzielnik ,dzielna;
        if(text=="")
            dzielna =0;
        else
            dzielna  = Float.parseFloat(text2);

        if (text2=="")
            dzielnik=1;
        else
            dzielnik = Float.parseFloat(text);

        if(dzielnik == 0){
            text = "Dziel. przez 0!";
        }else{
            text=String.valueOf(dzielna/dzielnik);
        }

    }
    private void bksp(){
        if(text.length()>0)
        text=text.substring(0,text.length()-1);
    }
    private void plusMinus(){
        if((text!=""&&(text!="+"))&&(text!="-")&&(text!="*")&&(text!="/")) {
            float sq = Float.parseFloat(text);
            sq *= -1;
            text=String.valueOf(sq);
        }
    }
    private void sqrt(){
        float liczba=0;
        if(text=="")
            liczba =0;
        else
            liczba  = Float.parseFloat(text);

        text=String.valueOf(Math.sqrt(liczba));
    }
    private void potega(){
        float liczba;
        if(text=="")
            liczba=0;
        else
            liczba = Float.parseFloat(text);
       text=String.valueOf(Math.pow(liczba,2));
    }
    private void potega2(){
        float liczba,liczba2;
        if(text=="")
            liczba=0;
        else
            liczba = Float.parseFloat(text);
        if(text2=="")
            liczba2=0;
        else
            liczba2 = Float.parseFloat(text2);
        text=String.valueOf(Math.pow(liczba2,liczba));
    }
    private void log(){
        float liczba;
        if(text=="")
            liczba=0;
        else
            liczba = Float.parseFloat(text);
        if(liczba>0)
            text=String.valueOf(Math.log(liczba));
    }
    private void sin(){
        float liczba;
        if(text=="")
            liczba=0;
        else
            liczba = Float.parseFloat(text);
        text=String.valueOf(Math.sin(Math.toRadians(liczba)));
    }
    private void cos(){
        float liczba;
        if(text=="")
            liczba=0;
        else
            liczba = Float.parseFloat(text);
        text=String.valueOf(Math.cos(Math.toRadians(liczba)));
    }
    private void tan(){
        float liczba;
        if(text=="")
            liczba=0;
        else
            liczba = Float.parseFloat(text);
        text=String.valueOf(Math.tan(Math.toRadians(liczba)));
    }
    @Override
    public void onClick (View v){
        switch(v.getId()) {
            case R.id.buttonAdvancedtan:
                tan();
                textview.setText(text);
                break;
            case R.id.buttonAdvancedcos:
                cos();
                textview.setText(text);
                break;

            case R.id.buttonAdvancedsin:
                sin();
                textview.setText(text);
                break;
            case R.id.buttonAdvancedln:
                log();
                textview.setText(text);
                break;

            case R.id.buttonAdvanced10:
                if(text==""){
                    text="0";
                }
                if(text.contains(".")){}
                else
                    text=text+".";
                textview.setText(text);
                break;

            case R.id.buttonAdvancedx2:
                potega();
                textview.setText(text);
                break;

            case R.id.buttonAdvancedxy:
                text2= (String) textview.getText();
                text="";
                znak = "^";
                textview.setText(text);
                break;

            case R.id.buttonAdvancedsqrt:
                sqrt();
                textview.setText(text);
                break;


            case R.id.buttonAdvanced18:
                plusMinus();
                textview.setText(text);
                break;

            case R.id.buttonAdvancedC:
                text="";
                text2="";
                textview.setText(text);
                break;

            case R.id.buttonAdvancedbksp:
                bksp();
                textview.setText(text);
                break;

            case R.id.buttonAdvanced11:
                switch(znak) {
                    case "+":
                        dodaj();
                        break;
                    case "-":
                        minus();
                        break;
                    case "*":
                        mnoz();
                        break;
                    case "/":
                        dziel();
                        break;
                    case "^":
                        potega2();
                        break;
                    default:
                        break;
                }
                textview.setText(text);
                break;
            case R.id.buttonAdvanceddziel:
                text2= (String) textview.getText();
                text="";
                znak = "/";
                textview.setText(btndziel.getText());
                break;
            case R.id.buttonAdvancedmnoz:
                text2= (String) textview.getText();
                text="";
                znak = "*";
                textview.setText(btnmnoz.getText());
                break;
            case R.id.buttonAdvancedminus:
                text2= (String) textview.getText();
                text="";
                znak = "-";
                textview.setText(btnminus.getText());
                break;
            case R.id.buttonAdvancedplus:
                text2= (String) textview.getText();
                text="";
                znak = "+";
                textview.setText(btnplus.getText());
                break;

            case R.id.buttonAdvanced0:
                text=text+btn.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced1:
                text=text+btn1.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced2:
                text=text+btn2.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced3:
                text=text+btn3.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced4:
                text=text+btn4.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced5:
                text=text+btn5.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced6:
                text=text+btn6.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced7:
                text=text+btn7.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced8:
                text=text+btn8.getText();
                textview.setText(text);
                break;
            case R.id.buttonAdvanced9:
                text=text+btn9.getText();
                textview.setText(text);
                break;

        }
    }
}
