package com.example.home.calc;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
public class FragmentSimple extends Fragment implements View.OnClickListener {
    private static TextView textview;
    View view;
    Button btn,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnplus,btn11,btnminus,btnmnoz,btndziel,btnbksp,btnC,btn18,btn10;
    String text="",text2="",znak="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.simple, container, false);
        textview=((TextView) view.findViewById(R.id.textSimple));

        btn = (Button)view.findViewById(R.id.buttonSimple0);
        btn.setOnClickListener(this);
        btn1 = (Button)view.findViewById(R.id.buttonSimple1);
        btn1.setOnClickListener(this);
        btn2 = (Button)view.findViewById(R.id.buttonSimple2);
        btn2.setOnClickListener(this);
        btn3 = (Button)view.findViewById(R.id.buttonSimple3);
        btn3.setOnClickListener(this);
        btn4 = (Button)view.findViewById(R.id.buttonSimple4);
        btn4.setOnClickListener(this);
        btn5 = (Button)view.findViewById(R.id.buttonSimple5);
        btn5.setOnClickListener(this);
        btn6 = (Button)view.findViewById(R.id.buttonSimple6);
        btn6.setOnClickListener(this);
        btn7 = (Button)view.findViewById(R.id.buttonSimple7);
        btn7.setOnClickListener(this);
        btn8 = (Button)view.findViewById(R.id.buttonSimple8);
        btn8.setOnClickListener(this);
        btn9 = (Button)view.findViewById(R.id.buttonSimple9);
        btn9.setOnClickListener(this);
        btnplus = (Button)view.findViewById(R.id.buttonSimplePlus);
        btnplus.setOnClickListener(this);
        btn11 = (Button)view.findViewById(R.id.buttonSimple11); //rowna sie
        btn11.setOnClickListener(this);
        btnminus = (Button)view.findViewById(R.id.buttonSimpleMinus);
        btnminus.setOnClickListener(this);
        btnmnoz = (Button)view.findViewById(R.id.buttonSimpleMnoz);
        btnmnoz.setOnClickListener(this);
        btndziel= (Button)view.findViewById(R.id.buttonSimpleDziel);
        btndziel.setOnClickListener(this);
        btnbksp= (Button)view.findViewById(R.id.buttonSimpleBksp);
        btnbksp.setOnClickListener(this);
        btnC= (Button)view.findViewById(R.id.buttonSimpleC);
        btnC.setOnClickListener(this);
        btn18= (Button)view.findViewById(R.id.buttonSimple18); //plus minus
        btn18.setOnClickListener(this);
        btn10= (Button)view.findViewById(R.id.buttonSimple10); //przecinek
        btn10.setOnClickListener(this);
        return view;
    }

    private void dodaj(){
        float skladnik1,skladnik2;
        if(text=="")
            skladnik1=0;
        else
            skladnik1 = Float.parseFloat(text);
        if (text2=="")
            skladnik2=0;
        else
            skladnik2 = Float.parseFloat(text2);

        text=String.valueOf(skladnik1+skladnik2);
    }
    private void minus(){
        float odjemna ,odjemnik;
        if(text=="")
            odjemna =0;
        else
            odjemna  = Float.parseFloat(text2);
        if (text2=="")
            odjemnik=0;
        else
            odjemnik = Float.parseFloat(text);

        text=String.valueOf(odjemna-odjemnik);
    }
    private void mnoz(){
        float czynnik1 ,czynnik2;
        if(text=="")
            czynnik1 =0;
        else
            czynnik1  =Float.parseFloat(text2);
        if (text2=="")
            czynnik2=0;
        else
            czynnik2 = Float.parseFloat(text);

        text=String.valueOf(czynnik1*czynnik2);
    }
    private void dziel(){
        float dzielnik ,dzielna;
        if(text=="")
            dzielna =0;
        else
            dzielna  = Float.parseFloat(text2);

        if (text2=="")
            dzielnik=1;
        else
            dzielnik = Float.parseFloat(text);

        if(dzielnik == 0){
            text = "Dziel. przez 0!";
        }else{
            text=String.valueOf(dzielna/dzielnik);
        }

    }
    private void bksp(){
        if(text.length()>0)
            text=text.substring(0,text.length()-1);
    }
    private void zmiana(){
        if((text!=""&&(text!="+"))&&(text!="-")&&(text!="*")&&(text!="/")) {
            float sq = Float.parseFloat(text);
            sq *= -1;
            text=String.valueOf(sq);
        }
    }
    @Override
    public void onClick (View v){
        switch(v.getId()) {
            case R.id.buttonSimple10:
                if(text==""){
                    text="0";
                }
                if(text.contains(".")){}
                else
                    text=text+".";
                textview.setText(text);
                break;
            case R.id.buttonSimple18:
                zmiana();
                textview.setText(text);
                break;

            case R.id.buttonSimpleC:
                text="";
                text2="";
                textview.setText(text);
                break;

            case R.id.buttonSimpleBksp:
                bksp();
                textview.setText(text);
                break;

            case R.id.buttonSimple11: //rowna sie
                switch(znak) {
                    case "+":
                        dodaj();
                        break;
                    case "-":
                        minus();
                        break;
                    case "*":
                        mnoz();
                        break;
                    case "/":
                        dziel();
                        break;
                    default:
                        break;
                }
                znak="";
                textview.setText(text);
                break;
            case R.id.buttonSimpleDziel:
                text2= (String) textview.getText();
                text="";
                znak = "/";
                textview.setText(btndziel.getText());
                break;
            case R.id.buttonSimpleMnoz:
                text2= (String) textview.getText();
                text="";
                znak = "*";
                textview.setText(btnmnoz.getText());
                break;
            case R.id.buttonSimpleMinus:
                text2= (String) textview.getText();
                text="";
                znak = "-";
                textview.setText(btnminus.getText());
                break;
            case R.id.buttonSimplePlus:
                text2= (String) textview.getText();
                text="";
                znak = "+";
                textview.setText(btnplus.getText());
                break;

            case R.id.buttonSimple0:
                text=text+btn.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple1:
                text=text+btn1.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple2:
                text=text+btn2.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple3:
                text=text+btn3.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple4:
                text=text+btn4.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple5:
                text=text+btn5.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple6:
                text=text+btn6.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple7:
                text=text+btn7.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple8:
                text=text+btn8.getText();
                textview.setText(text);
                break;
            case R.id.buttonSimple9:
                text=text+btn9.getText();
                textview.setText(text);
                break;
            default:
                textview.setText("Error");
                break;

        }
    }
}
